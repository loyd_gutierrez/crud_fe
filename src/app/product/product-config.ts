import { ConfigFormModel, ConfigTableModel } from 'app/shared/interfaces/config';
import { Validators } from '@angular/forms';

export const productFormConfig: ConfigFormModel = {
  model_id: 'product_form',
  configs: [
    {
      control: 'id',
      controlType: 'hidden',
    },
    {
      control: 'name',
      controlType: 'text',
      label: 'Product Name',
      placeholder: '',
      validations: [ Validators.required ]
    },
    {
      control: 'price',
      controlType: 'text',
      label: 'Price',
      placeholder: '',
      validations: [ Validators.required ]
    },
    {
      control: 'manufactured_date',
      controlType: 'date',
      label: 'Date Manufactured',
      placeholder: '',
      validations: [ Validators.required ]
    },
    {
      control: 'location',
      label: 'Location',
      placeholder: '',
      controlType: 'textarea'
    },
    {
      control: 'is_available',
      label: 'Avialable',
      controlType: 'checkbox'
    }
  ]
}

export const productTableConfig: ConfigTableModel = {
  model_id: 'product_table',
  header: [
    'Name',
    'Price',
    'Location',
    'Date Manufactured'
  ],
  configs: [
    { key: 'id', label: '' },
    { key: 'is_active', label: '' },
    { key: 'name', label: 'Name' },
    { key: 'price', label: 'Price' },
    { key: 'manufactured_date', label: 'Date Manufactured' },
    { key: 'location', label: 'Location' }
  ],
  sortBy: {
    column: 'name',
    direction: 'asc'
  }
}
