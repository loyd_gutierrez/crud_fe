import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigFormModel } from 'app/shared/interfaces/config';
import { productFormConfig } from '../product-config';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.scss']
})
export class UpdateProductComponent implements OnInit {

  apiUrl: string;
  user_id: string;
  valuesDownloaded = false;
  form: FormGroup;
  messageSuccess: string;
  messageDanger: string;
  configFormModel: ConfigFormModel;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.configFormModel = productFormConfig;
    this.form = new FormGroup({});
    this.user_id = this.route.snapshot.paramMap.get('id');
    this.apiUrl = environment.apiUrl + 'product/' + this.user_id;
    this.http.get(this.apiUrl).subscribe((res) => {
      this.configFormModel.configs.forEach(config => {
        config.value = res[config.control];
      });
      this.valuesDownloaded = true;
    }, err => {
      this.messageDanger = 'User is not found!';
    });
  }

  submitForm(params) {
    this.http.put(this.apiUrl, (params)).subscribe((res) => {
      this.messageSuccess = 'Your user has been added!';
    },
    err => {
      this.messageDanger = err.statusText;
    });
  }
}
