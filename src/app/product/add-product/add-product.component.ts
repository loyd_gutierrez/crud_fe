import { Component, OnInit } from '@angular/core';
import { ConfigFormModel } from 'app/shared/interfaces/config';
import { productFormConfig } from '../product-config';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  form: FormGroup;
  messageSuccess: string;
  messageDanger: string;
  configFormModel: ConfigFormModel;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.configFormModel = productFormConfig;
    this.form = new FormGroup({});
  }

  submitForm(params) {
    const url = environment.apiUrl + 'product';
    this.http.post(url, (params)).subscribe((res) => {
      this.messageSuccess = 'Your user has been added!';
      this.form.reset();
    },
    err => {
      this.messageDanger = err.statusText;
    });
  }
}
