import { Component, OnInit } from '@angular/core';
import { ConfigTableModel, TableConfig } from 'app/shared/interfaces/config';
import { productTableConfig } from './product-config';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  configTableModel: ConfigTableModel;
  errorMessage: string;
  data;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.configTableModel = productTableConfig;
    this.getProducts();
  }

  getProducts() {
    const url = environment.apiUrl + 'product';
    this.http.get(url).subscribe((res) => {
      this.data = res;
    },
    err => {
      this.errorMessage = err.statusText;
    });
  }

  searchKey(key: string) {
    const url = environment.apiUrl + 'product?q=' + key;
    this.http.get(url)
      .subscribe((res) => {
        this.data = res;
      }, err => {
        console.log(err);
      });
  }

  scrollByColumn(sortedBy) {
    const url = environment.apiUrl + 'product?sort=' + sortedBy.column + '&by=' + sortedBy.direction;
    this.http.get(url)
      .subscribe((res) => {
        this.data = res;
      }, err => {
        console.log(err);
      });
  }

  deleteById(id: string) {
    const url = environment.apiUrl + 'product/' + id;
    this.http.delete(url)
      .subscribe((res) => {
        this.data = res;
      }, err => {
        console.log(err);
      });
  }
}
