import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { AddProductComponent } from './add-product/add-product.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { RouterModule } from '@angular/router';
import { SharedComponentsModule } from 'app/shared/components/shared-components.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedComponentsModule
  ],
  declarations: [
    ProductComponent,
    AddProductComponent,
    UpdateProductComponent
  ],
  exports: [
    CommonModule,
    ProductComponent,
    AddProductComponent,
    UpdateProductComponent
  ]
})
export class ProductModule { }
