import { Component, OnInit } from '@angular/core';
import { ConfigFormModel } from 'app/shared/interfaces/config';
import { customerFormConfig } from '../customer-config';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {

  form: FormGroup;
  messageSuccess: string;
  messageDanger: string;
  configFormModel: ConfigFormModel;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.configFormModel = customerFormConfig;
    this.form = new FormGroup({});
  }

  submitForm(params) {
    const url = environment.apiUrl + 'customer';
    this.http.post(url, (params)).subscribe((res) => {
      this.messageSuccess = 'Your user has been added!';
      this.form.reset();
    },
    err => {
      this.messageDanger = err.statusText;
    });
  }
}
