import { Component, OnInit } from '@angular/core';
import { ConfigTableModel, TableConfig } from 'app/shared/interfaces/config';
import { customerTableConfig } from './customer-config';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  configTableModel: ConfigTableModel;
  errorMessage: string;
  data;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.configTableModel = customerTableConfig;
    this.getCustomers();
  }

  getCustomers() {
    const url = environment.apiUrl + 'customer';
    this.http.get(url).subscribe((res) => {
      this.data = res;
    },
    err => {
      this.errorMessage = err.statusText;
    });
  }

  searchKey(key: string) {
    const url = environment.apiUrl + 'customer?q=' + key;
    this.http.get(url)
      .subscribe((res) => {
        this.data = res;
      }, err => {
        console.log(err);
      });
  }

  scrollByColumn(sortedBy) {
    const url = environment.apiUrl + 'customer?sort=' + sortedBy.column + '&by=' + sortedBy.direction;
    this.http.get(url)
      .subscribe((res) => {
        this.data = res;
      }, err => {
        console.log(err);
      });
  }

  deleteById(id: string) {
    const url = environment.apiUrl + 'customer/' + id;
    this.http.delete(url)
      .subscribe((res) => {
        this.data = res;
      }, err => {
        console.log(err);
      });
  }
}
