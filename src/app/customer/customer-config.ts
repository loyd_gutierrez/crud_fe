import { ConfigFormModel, ConfigTableModel } from 'app/shared/interfaces/config';
import { Validators } from '@angular/forms';

export const customerFormConfig: ConfigFormModel = {
  model_id: 'customer_form',
  configs: [
    {
      control: 'id',
      controlType: 'hidden',
    },
    {
      control: 'name',
      controlType: 'text',
      label: 'Full name',
      placeholder: '',
      validations: [ Validators.required ]
    },
    {
      control: 'address',
      controlType: 'textarea',
      label: 'Address',
      placeholder: '',
      validations: [ Validators.required ]
    },
    {
      control: 'phone',
      controlType: 'text',
      label: 'Phone number',
      placeholder: '+639151234566',
      validations: [ Validators.required ]
    },
    {
      control: 'member_since_date',
      label: 'Membership date',
      controlType: 'date'
    },
    {
      control: 'is_active',
      label: 'Active',
      controlType: 'checkbox'
    }
  ]
}

export const customerTableConfig: ConfigTableModel = {
  model_id: 'customer_table',
  header: [
    'Name',
    'Phone',
    'Address',
    'Membership Date'
  ],
  configs: [
    { key: 'id', label: '' },
    { key: 'is_active', label: '' },
    { key: 'name', label: 'Name' },
    { key: 'phone', label: 'Phone' },
    { key: 'address', label: 'Address' },
    { key: 'member_since_date', label: 'Membership Date' }
  ],
  sortBy: {
    column: 'name',
    direction: 'asc'
  }
}
