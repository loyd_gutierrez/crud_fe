import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerComponent } from './customer.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { UpdateCustomerComponent } from './update-customer/update-customer.component';
import { RouterModule } from '@angular/router';
import { SharedComponentsModule } from 'app/shared/components/shared-components.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedComponentsModule,
  ],
  declarations: [
    CustomerComponent,
    AddCustomerComponent,
    UpdateCustomerComponent
  ],
  exports: [
    CommonModule,
    CustomerComponent,
    AddCustomerComponent,
    UpdateCustomerComponent
  ]
})
export class CustomerModule { }
