import { Component, Input, Output, OnInit, EventEmitter  } from '@angular/core';
import { ConfigFormModel, FormConfig } from 'app/shared/interfaces/config';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @Output() submitForm = new EventEmitter<string[]>();

  @Input() form: FormGroup;
  @Input() model_id: string;
  @Input() configs: FormConfig[];

  constructor() { }

  ngOnInit() {
    this.configs.forEach((config: FormConfig) => {
      console.log(config.value);

      this.form.addControl(config.control, new FormControl(
        config.value || '',
        config.validations ? config.validations : null
      ));
    });
  }

  onClickSubmit() {
    this.submitForm.emit(this.form.value);
  }
}
