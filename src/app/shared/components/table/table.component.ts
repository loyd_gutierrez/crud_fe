import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TableConfig } from 'app/shared/interfaces/config';
import { SortBy } from 'app/shared/interfaces/sort';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Output() searchKey = new EventEmitter<string>();
  @Output() deleteById = new EventEmitter<string>();
  @Output() scrollByColumn = new EventEmitter<SortBy>();

  @Input() datas: Object[];
  @Input() addApiUrl: string;
  @Input() updateApiUrl: string;
  @Input() headers: string[];
  @Input() configs: TableConfig[];
  @Input() sortBy: SortBy;

  constructor() { }

  ngOnInit() { }

  sortColumn(key: string) {
    const filtered = this.configs.filter(config => config.label === key);
    let orderBy = 'asc';

    if (filtered[0].key === this.sortBy.column) {
      orderBy = this.sortBy.direction === 'asc' ? 'desc' : 'asc';
    }

    this.sortBy = {
      column: filtered[0].key,
      direction: orderBy
    };

    this.scrollByColumn.emit({
      column: filtered[0].key,
      direction: orderBy
    });
  }

  onInputChange(event: KeyboardEvent) {
    this.searchKey.emit((<HTMLInputElement>event.target).value);
  }

  confirmDelete(id: string, name: string) {
    // Lack of time... sorry :D
    if (confirm(`Are you sure you want to delete ${name}?`)) {
      this.deleteById.emit(id);
    }
  }

  showColumn(header: string, direction: string) {
    const filtered = this.configs.filter(config => config.label === header);
    return filtered[0].key === this.sortBy.column && direction === this.sortBy.direction;
  }
}
