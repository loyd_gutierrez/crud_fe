import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { TableComponent } from './table/table.component';
import { TitleComponent } from './title/title.component';
import { FormComponent } from './form/form.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    NavComponent,
    TableComponent,
    TitleComponent,
    FormComponent
  ],
  exports: [
    NavComponent,
    TableComponent,
    TitleComponent,
    FormComponent,
  ]
})
export class SharedComponentsModule { }
