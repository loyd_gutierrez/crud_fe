import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedComponentsModule } from 'app/shared/components/shared-components.module';

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule
  ],
  declarations: [],
  exports: [
    SharedComponentsModule
  ]
})
export class SharedModule { }
