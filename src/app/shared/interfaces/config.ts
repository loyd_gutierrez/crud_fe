import { SortBy } from './sort';

interface ConfigFormModel {
  model_id: string;
  configs: FormConfig[];
}

interface FormConfig {
  control: string;
  controlType: string;
  value?: any;
  label?: string;
  placeholder?: string;
  validations?: any[],
  class?: string;
}

interface ValidatorType {
  type: string;
  value?: any;
};

interface ConfigTableModel {
  model_id: string;
  header: string[];
  configs: TableConfig[];
  sortBy: SortBy
}

interface TableConfig {
  key: string,
  label: string
}

export {
  ConfigFormModel,
  FormConfig,
  ValidatorType,

  ConfigTableModel,
  TableConfig
}
