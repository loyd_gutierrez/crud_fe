interface SortBy {
  column: string;
  direction: string;
}

export {
  SortBy
}
